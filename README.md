# Better than humans? Machine learning-based anatomy recognition in minimally-invasive abdominal surgery



## Soon to come

This repository will contain the source code of the methods used in the paper "Better than humans? Machine learning-based anatomy recognition in minimally-invasive abdominal surgery" which can be found [here](https://www.medrxiv.org/content/10.1101/2022.11.11.22282215v1).
